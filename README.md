# vue-wf

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c99e092373364f12b9a105c671f1cbdd)](https://app.codacy.com/app/Bitti09/vue-wf?utm_source=github.com&utm_medium=referral&utm_content=Bitti09/vue-wf&utm_campaign=Badge_Grade_Dashboard)
[![Build Status](https://travis-ci.com/Bitti09/vue-wf.svg?branch=master)](https://travis-ci.com/Bitti09/vue-wf)

> Notification Part will be extended over next couple of days.  
> Currently there **_aren't_** any automatic Push Notifications on new Alerts etc.  
> System used for Notifications: Firebase Cloud Messages
